/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionbancaire;

import java.util.ArrayList;

/**
 *
 * @author Hp
 */
public class Compte {
    private int numCompte;
    private Client idClient;
    private float soldeCompte;
    private float tauxInteret;
  
    public Compte(int numComptep, float soldep, float tauxInteretp, Client idClientp) {
        this.numCompte = numComptep;
        this.soldeCompte = soldep;
        this.tauxInteret = tauxInteretp;
        this.idClient = idClientp;
    }
    

    //getters et setters
     /**
     * @return the idClient
     */
    public Client getIdClient() {
        return idClient;
    }

    /**
     * @param idClient the idClient to set
     */
    public void setIdClient(Client idClient) {
        this.idClient = idClient;
    }
  
    public float getSoldeCompte() {
        return soldeCompte;
    }

    public void setSoldeCompte(float soldeCompte) {
        this.soldeCompte = soldeCompte;
    }

    public float getTauxInteret() {
        return tauxInteret;
    }

    public void setTauxInteret(float tauxInteret) {
        this.tauxInteret = tauxInteret;
    }
    /**
     * @return the numCompte
     */
    public int getNumCompte() {
        return numCompte;
    }

    /**
     * @param numCompte the numCompte to set
     */
    public void setNumCompte(int numCompte) {
        this.numCompte = numCompte;
    }
}
